#--- Mise à jour du répertoire apt
apt update

#--- Install Open JDK 11
apt install openjdk-11-jdk

#--- Script pour installer npm
#--- linux : sudo apt install npm
apt install npm

#--- Install node
apt install nodejs

#-- Checker si l'installation s'est bien déroulée
node -v or node –version

#--- Installer Angular-CLI
npm install -g @angular/cli

#--- Installation de Docker
apt-get remove docker docker-engine docker.io containerd runc

#--- Mettre à jour le package apt
apt-get update

apt-get install \
apt-transport-https \
ca-certificates \
curl \
gnupg-agent \
software-properties-common

#--- Ajouter la clé GPG
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#--- Verifier si le fingerprint est égal à 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88 en vérifiant les 8 derniers charactères
apt-key fingerprint 0EBFCD88

#--- Récupèrer la dérnière version stable
add-apt-repository \
"deb [arch=amd64] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) \
stable"

#--- Installer docker
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
apt install docker-compose

#--- Installation Jenkins
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'

apt-get update

apt-get install jenkins

systemctl start jenkins

systemctl status jenkins

groupadd docker
usermod -aG docker $USER
usermod -aG docker jenkins



#--- Purge des dépendances inutiles
apt autoremove
